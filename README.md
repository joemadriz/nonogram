# Random Angular Nonogram
Simple random nonogram game developed with Angular 8 and Angular Material. Full support for desktop and mobile devices.
Game creates a random board or chooses one from data base, if user completes a new board, this board and score are added to data base, if user completes an existing board, game updates scores from this data base board.
Check out the [demo](https://nonogram-fb406.firebaseapp.com/).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Language support
Game supports english and spanish. 
The application detects the browser language, any language other than Spanish or English is redirected to the English language. 
To add a new language, duplicate en.json, translate it and add a condition at the TranslationsService first if loop.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
