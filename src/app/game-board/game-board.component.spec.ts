import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyGameComponent } from './game-board.component';

describe('EasyGameComponent', () => {
  let component: EasyGameComponent;
  let fixture: ComponentFixture<EasyGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
