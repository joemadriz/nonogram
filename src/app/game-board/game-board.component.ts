import { Component, OnInit, ViewChild, ViewChildren, Renderer2, QueryList} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// * Material components 
import { MatSlideToggle, MatSnackBar, MatSnackBarConfig, MatDialog } from '@angular/material';
import { ResultDialogComponent } from '../result-dialog/result-dialog.component';
import { TimeCounterComponent } from '../time-counter/time-counter.component';

// * Services:
import { TranslationsService } from '../services/translations.service';
import { FirebaseService } from '../services/firebase.service'
import { PropertiesService } from '../services/properties.service';
import { ConfigService } from '../services/config.service';

export interface DialogData {
  title: string;
  message: string;
  rating: string;
  time: string;
  board: string;
  hasWon: boolean;
  type: string;
  hasBeenScored: boolean;
  boardID: string;
}

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',  
  styleUrls: ['./game-board.component.scss'],
  // * PropertiesService added at component as provider insted of at app.module.ts, 
  // * because we want to reset all properties when GameBoardComponent is initiated.
  providers: [ PropertiesService ],
  host: {
    '(window:resize)': 'onResize()'
  }
})

export class GameBoardComponent implements OnInit {

  @ViewChild(MatSlideToggle, {static: false}) slider: MatSlideToggle;
  @ViewChild(TimeCounterComponent, {static: false}) timeCounterComponent;
  @ViewChildren('boardTile') boardTile: QueryList<any>;
  @ViewChildren('live') live: QueryList<any>;

  constructor(
    private _render:Renderer2,
    private _snackBar: MatSnackBar,
    private _route: ActivatedRoute, 
    private _configService: ConfigService,
    private _language: TranslationsService,
    private _firebase: FirebaseService,
    private _propertiesService: PropertiesService,
    private _router: Router,
    public dialog: MatDialog  
  ) { }

  ngOnInit() {

    let counter = 0;
    this.initEnviroment(counter);
    this._configService.loadConfig();

  }

  /**
   * * Inits some required properties.
   * @param {Number} counter  Rows or cols number, initial value 0
   */
  initEnviroment(counter) {

    // * Detects if is mobile
    this._propertiesService.isMobile = window.innerWidth < 425 ? true : false;
    this._render.addClass(document.querySelector('.toolbar'), 'hide');

    // *Gets type of difficulty from url param.
    this._propertiesService.gameType = this._route.snapshot.paramMap.get('type');
    this._propertiesService.totalNumberOfTiles = this._propertiesService.gameType === 'easy' ? 100 : 225; 
    
    counter = this._propertiesService.gameType === 'easy' ? 9 : 14;

    this._propertiesService.columns = counter + 1;
    this._propertiesService.rows = counter + 1;

    // * Sets rowHeight for clues grids. 
    if(this._propertiesService.isMobile) {
      this._propertiesService.upperCluesRowHeight = this._propertiesService.gameType === 'easy' ? '1:2.5' : '1:5';
      this._propertiesService.leftCluesRowHeight = this._propertiesService.gameType === 'easy' ? '2.7:1.24' : '3.2:0.958';
    } else {
      this._propertiesService.upperCluesRowHeight = this._propertiesService.gameType === 'easy' ? '1:2' : '1:4';
      this._propertiesService.leftCluesRowHeight = this._propertiesService.gameType === 'easy' ? '2.5:1.42' : '3:1.118';
    }

    this.checkBoardGame();
    this.createBoard(counter);
  }

  /**
   * * Checks if board exists or not.
   */
  checkBoardGame() {
    // * Gets boards from data base
    this._firebase.getBoards().valueChanges().subscribe(board => {
      this._propertiesService.boards = board;
      let scoredBoard = false;
      this._propertiesService.boards.map((board, index)=> {
        let isEqual = board.value.equals(this._propertiesService.allIdsArray);
        if(isEqual) {
          scoredBoard = true;
          this._propertiesService.isActualBoardScored = true;
        }
        // * Add score and board to data base
        // ! Game only adds a new board when is scored
        // ! Use ONLY if your board database is empty
        if(index === this._propertiesService.boards.length -1 && this._configService.addBoardToDBatInit) {
          if(scoredBoard === false) {
            const board = {
              value: this._propertiesService.allIdsArray,
              type: this._propertiesService.gameType,
              users: [
                {
                  name: 'COM',
                  rating: 2,
                  time: '14:45'
                }
              ]
            }
            this._firebase.addBoard(board);
          }
        }
      });
    });
  }

  /**
   * * Creates a random board or chooses one from database randomly.
   * @param {Number} counter  Rows or cols counter variable 10 | 15
   */
  createBoard(counter) {

    const randNum = Math.round(Math.random() * 1);
    let newBoard = randNum === 0 ? false : true;

    if(newBoard) {
      // * Randomly gives type to tiles, values: 0 => void, 1 => fill.
      for(let index = 0; index < this._propertiesService.totalNumberOfTiles; index++) {
        const randomNum = Math.round(Math.random() * 1);
        this._propertiesService.boardArray.push(
          {
            id: randomNum,
            class: randomNum === 1 ? 'fill' : 'void'
          }
        );
      }

      this._propertiesService.boardArray.map((item) => {
        this._propertiesService.allIdsArray.push(item.id);
      });
      this.constructBoard(counter);

    }
    else {
      this._propertiesService.isActualBoardScored = true;
      // * Gets boards from data base
      const boardsByType = [];
      let boardsLength = 0;
      new Promise(resolve => {
        this._firebase.getBoards().valueChanges().subscribe(board => {
          board.map(item => {
            // * Chooses boards by their type easy | hard
            if(item['type'] === this._propertiesService.gameType) {
              boardsByType.push(item);
            }
          });
          boardsLength = boardsByType.length;
          resolve();
        });  
      }).then(() =>{
        // * Chooses a stored board randomly
        const boardLengthRandomNum = Math.round(Math.random() * (boardsLength - 1));
        const chosenBoard = boardsByType[boardLengthRandomNum];

        this._propertiesService.allIdsArray = chosenBoard.value;
        this._propertiesService.boardID = chosenBoard.id;
        chosenBoard.value.map(item => {
          this._propertiesService.boardArray.push(
            {
              id: item,
              class: item === 1 ? 'fill' : 'void'
            }
          );
        });
        this.constructBoard(counter);
      }); 
    }
  }
  /**
   * * Constructs board.
   * @param {Number} counter  Rows or cols counter variable 10 | 15
   */
  constructBoard(counter) {

    let loopCounter = this._propertiesService.gameType === 'easy' ? 9 : 14;
    for(let i = 0; i <= loopCounter;i ++) {
  
      // * Creates dynamic columns and rows arrays.
      this[`rowCounter${i}`] = counter;
      counter = counter + (this._propertiesService.gameType === 'easy' ? 10 : 15);
      this._propertiesService.rowCounterArray.push(this[`rowCounter${i}`]);
      this._propertiesService.colArray.push(this[`colArray${i}`] = []);
      this._propertiesService.rowArray.push(this[`rowArray${i}`] = []);
      this._propertiesService.allIndexRowArray.push(this[`indexRowArray${i}`] = []);
      this._propertiesService.allIndexColArray.push(this[`indexColArray${i}`] = []);

    }

    let idCounter = this._propertiesService.gameType === 'easy' ? 9 : 14;
    let arrCounter = 0;
    
    // * Rows array. 
    this._propertiesService.allIdsArray.map((item, index) => {

      if(index <= idCounter) {
        this[`rowArray${arrCounter}`].push(item);
        this[`indexRowArray${arrCounter}`].push(index);
        if(index === idCounter) {
          idCounter = idCounter + (this._propertiesService.gameType === 'easy' ? 10 : 15);
          arrCounter++;
        }
      }
    });

    let count = this._propertiesService.columns;
    let firsTime = false;

    // * Columns array.
    this._propertiesService.rowArray.map((item, index) => {
      item.map((it, indx) => {
        this[`colArray${indx}`].push(it);
        this._propertiesService.allIndexColArray.map((col, ix) =>{
          if(index === indx) {
            if(!firsTime) {
              this[`indexColArray${indx}`].push(index);
              firsTime = true;
            }
            else {
              this[`indexColArray${indx}`].push(count);
              count = count + this._propertiesService.columns;
            }
            if(ix == this._propertiesService.columns -1) {
              count = indx +1;
            }
          }
        });
      });
    });

    // * Calculates clues group tiles on rows.
    this.calculateClues(this._propertiesService.rowArray, 'row');

    // * Calculates clues group tiles on columns.
    this.calculateClues(this._propertiesService.colArray, 'col');

    this._propertiesService.isBoardReady = true;
  }


  /**
   * * Calculates clues group tiles on columns and rows
   * @param {Object} itemToIterate  Object to iterate this._propertiesService.rowArray | this._propertiesService.colArray
   * @param {String} type           Type of item to iterate 'col' | 'row'
   */
  calculateClues(itemToIterate, type) {

    itemToIterate.map((item, index) => {

      type === 'row' ? this[`rowArray${index}`] = [] : this[`colArray${index}`] = [];
      let oldVal = 0;
      let actualVal = 0;

      item.map((it, indx) => {

        if(oldVal === 0) {
          // * Gets in only if old value is 0.
          oldVal = it;
          actualVal = actualVal + it;
          if(indx === item.length -1 && it !== 0) {
            type === 'row' ? this[`rowArray${index}`].push(actualVal) : this[`colArray${index}`].push(actualVal);
          }
        }
        else if(oldVal !== 0 && it !== 0) {
          // * Gets in only if old value is 1.
          actualVal = actualVal + it;
          if(indx === item.length -1) {
            type === 'row' ? this[`rowArray${index}`].push(actualVal) : this[`colArray${index}`].push(actualVal);
          }
        }
        else if(it === 0 && oldVal === 1) {
          type === 'row' ? this[`rowArray${index}`].push(actualVal) : this[`colArray${index}`].push(actualVal);
          actualVal = 0;
          oldVal = it;
        }
      });  

      type === 'row' ? this._propertiesService.rowResutArray.push(this[`rowArray${index}`]) : this._propertiesService.colResultArray.push(this[`colArray${index}`]) ;
      
    });
  }

  /**
   * * Re-styles the clues tiles on window resize.
   */
  onResize() {
    // * Detects if is mobile
    this._propertiesService.isMobile = window.innerWidth < 425 ? true : false;
    if(this._propertiesService.isMobile) {
      this._render.addClass(document.querySelector('.toolbar'), 'hide');
    }
    // * Sets rowHeight for clues grids. 
    if(this._propertiesService.isMobile) {
      this._propertiesService.upperCluesRowHeight = this._propertiesService.gameType === 'easy' ? '1:2.3' : '1:5';
      this._propertiesService.leftCluesRowHeight = this._propertiesService.gameType === 'easy' ? '2.7:1.22995' : '3.2:0.958';
    } else {
      this._propertiesService.upperCluesRowHeight = this._propertiesService.gameType === 'easy' ? '1:2' : '1:4';
      this._propertiesService.leftCluesRowHeight = this._propertiesService.gameType === 'easy' ? '2.5:1.42' : '3:1.118';
    }
  }

 /**
   * * Highlights the row and column at cursor position.
   * @param {$event} event    Event of mouse enter or mouse leave
   * @param {String} eventType     Type of event, enter or mouse leave 'enter' | 'leave'
   */
  highLightColsRows(event, eventType) {

    if(!this._propertiesService.isMobile) {

      const hoveredTileId = parseInt(event.path[0].id);
      let rowTilesToHighLight = [];
      let colTilesToHighLight = [];
      const constructArrayTilesToHighLight = (item, tileType) => {
        if(hoveredTileId === item) {
          tileType.map(it =>{
            rowTilesToHighLight.push(it);
          }); 
        }
      }
      const highlightClues = (clue, eventType) => {
        eventType === 'enter' ? this._render.addClass(clue, 'highlihted') : this._render.removeClass(clue, 'highlihted');
      }
      const constructCluesToHightLight = (type) => {
        // * Easy game, 10 cols & rows
        if(this._propertiesService.gameType === 'easy') {
          const clueID = type === 'row' 
            ? (hoveredTileId.toString().length === 1 ? 0 : hoveredTileId.toString().slice(0,1))
            : (hoveredTileId.toString().length === 1 ? hoveredTileId : hoveredTileId.toString().slice(1,2));
          const clue = type === 'row' ? document.querySelector(`#leftClues${clueID}`) : document.querySelector(`#upperClues${clueID}`);
          highlightClues(clue, eventType);   
        } 
        // * Hard game, 15 cols & rows
        else {
          let oldRowCounterValue = 0;
          let oldColCounterValue = 0;
          let enterOnce = true;
          let entersOnce = true;
          let counter = 15;
          for(let i = 0; i < 15; i++) {
    
            if(type === 'row' && hoveredTileId >= oldRowCounterValue && hoveredTileId <= this[`rowCounter${i}`] && enterOnce) {
              const clue = document.querySelector(`#leftClues${i}`);
              highlightClues(clue, eventType);
              oldRowCounterValue = this[`rowCounter${i}`] ;
              enterOnce = false;
            }
            if(type === 'row' && hoveredTileId >= oldColCounterValue && hoveredTileId <= this[`rowCounter${i}`]) {
              if(i === 0) {
                const clue = document.querySelector(`#upperClues${hoveredTileId}`);
                highlightClues(clue, eventType);
              }
              else {
                if(i === 1 && hoveredTileId >= 15) {
                  let calculate = hoveredTileId - counter;
                  const clue = document.querySelector(`#upperClues${calculate}`);
                  highlightClues(clue, eventType);
                }
                else if(i!== 0 && i !== 1 && hoveredTileId >= 30 && entersOnce) {
                  let calculate = hoveredTileId - (counter * i);
                  const clue = document.querySelector(`#upperClues${calculate}`);
                  highlightClues(clue, eventType);
                  entersOnce = false;
                }
              }
            }
          }
        }
      }

      // * Select rows to highlight:
      this._propertiesService.allIndexRowArray.map((item, index) => {
        item.map(item2 =>{
          constructArrayTilesToHighLight(item2, this[`indexRowArray${index}`]);
          constructCluesToHightLight('row');
        });
      });

      // * Select cols to highlight:
      this._propertiesService.allIndexColArray.map((item, index) => {
        item.map(item2 =>{
          constructArrayTilesToHighLight(item2, this[`indexColArray${index}`]);
          constructCluesToHightLight('col');
        });
      });

      // * Highligth tiles:
      this.boardTile.map((tile, index) =>{
        const containsFillClass = tile._element.nativeElement.classList.contains('fill');
        const containsVoidClass = tile._element.nativeElement.classList.contains('void');
        const addRemoveClass = (item) => {
          if(item === index) {
            if(!containsFillClass && !containsVoidClass) {
              eventType === 'enter' ? this._render.addClass(tile._element.nativeElement, 'hover') : this._render.removeClass(tile._element.nativeElement, 'hover');
            }
          }
        }
        rowTilesToHighLight.map(item =>{
          addRemoveClass(item);
        });
        colTilesToHighLight.map(item =>{
          addRemoveClass(item);
        });
      });
    }
  }
  
  /**
   * * Checks clicked tile and if is correct.
   * @param {$event} event Event from user click
   * @param {Number} type  Type of tile 0 | 1
   */
  tileClicked(event, type) {
 
    let classToApply = 'fill';
    const tile = event.path[1];
    const tileClickedId = parseInt(tile.id);

    // * Avoid tiles been clicked more than once.
    if(tile.dataset.clicked == 'false') {

      tile.dataset.clicked = 'true';        
      classToApply = this._propertiesService.isSliderChecked ? 'fill' : 'void';
      this._render.addClass(tile, classToApply);

      // * Call to check if game is finished.
      this.checkIfFinished();
      if((type === 0 && !this._propertiesService.isSliderChecked) || (type === 1 && this._propertiesService.isSliderChecked)){
        // * Correct choose.
        this.openSnackBar(this._language.translations.boardGame.snackBar.success, null, 'success');
      }
      else {
        // * Error choose.
        let enterOneTime = false;
        let numOfEnterTimes = 0;

        this.live.map((item)=>{
          numOfEnterTimes++;
          if(!item._elementRef.nativeElement.classList.contains('consumed') && enterOneTime === false) {
            this._render.addClass(item._elementRef.nativeElement, 'consumed');
            enterOneTime = true;
          }
          if(numOfEnterTimes === 3 && item._elementRef.nativeElement.classList.contains('consumed')) {
            this._propertiesService.timerRunning = false;
            this.openSnackBar(this._language.translations.boardGame.snackBar.error, null, 'error');
            let message = this._propertiesService.gameType === 'easy' ? this._language.translations.boardGame.dialog.lose.easyMessage : this._language.translations.boardGame.dialog.lose.hardMessage;
            this.openDialog(this._language.translations.boardGame.dialog.lose.title, message, false);
          }
        });
        this.openSnackBar(this._language.translations.boardGame.snackBar.error, null, 'error');
        this._propertiesService.rating.pop();
        
        this._render.addClass(tile, 'error');
        setTimeout(() => {
          if(type === 0) {
            this._render.addClass(tile, 'void');
            this._render.removeClass(tile, 'fill');
            this._render.removeClass(tile, 'error');
          }
          else {
            this._render.addClass(tile, 'fill');
            this._render.removeClass(tile, 'void');
            this._render.removeClass(tile, 'error');
          }
        }, 100);
      }

      this.checkIfRowCompleted(tileClickedId).then(() => this.checkIfColCompleted(tileClickedId));
    } 
  }

  /**
   * * Checks if row is complete.
   * @param {Number} tileClickedId Id from tile clicked
   */
  checkIfRowCompleted(tileClickedId) {
    return new Promise((resolve) => {
      let rowTilesToCheck = [];
      let totalRowClues = 0;
      let totalRowTilesClickedOk = 0;
      // * Select rows to check if completed:
      this._propertiesService.allIndexRowArray.map((item, index) => {
        this.setCluesCompleted(tileClickedId, 'row', item, index, rowTilesToCheck, totalRowClues, totalRowTilesClickedOk);
      });
      resolve();
    });
  }

  /**
   * * Checks if column is complete.
   * @param {Number} tileClickedId Id from tile clicked
   */
  checkIfColCompleted(tileClickedId) { 
    let colTilesToCheck = [];
    let totalColClues = 0;
    let totalColTilesClickedOk = 0;
    // * Select cols to check if completed:
    this._propertiesService.allIndexColArray.map((item, index) => {
      this.setCluesCompleted(tileClickedId, 'col', item, index, colTilesToCheck, totalColClues, totalColTilesClickedOk);
    });
  }

  /**
   * * Shades rows & cols clues if completed.
   * @param {Number} tileClickedId       Id from tile clicked
   * @param {String} type                Type of array 'row' | 'col'
   * @param {Object} item                Object to iterate
   * @param {Number} index               Index of item
   * @param {Object} tilesToCheck        Tiles to check
   * @param {Number} totalClues          Total clues 
   * @param {Number} totalTilesClickedOk Total of correct clicked tiles
   */
  setCluesCompleted(tileClickedId, type, item, index, tilesToCheck, totalClues, totalTilesClickedOk) { 

    let arrayToIterate = type === 'row' ? this[`rowArray${index}`] : this[`colArray${index}`];
    let indexArrrayToIterate = type === 'row' ? this[`indexRowArray${index}`] : this[`indexColArray${index}`]
    this._propertiesService;
    item.map((item2) =>{
      if(tileClickedId === item2) {
        arrayToIterate.map(it =>{
          totalClues = totalClues + it;
        });
        indexArrrayToIterate.map(it =>{
          tilesToCheck.push(it);
        });
        tilesToCheck.map(tile =>{
          // * This setTimeout is because there is another setTimeout to mark the errors in red and later mark the correct tile
          setTimeout(() => {
            const select = document.getElementById(tile);
            if(select.dataset.clicked && select.classList.contains('fill') && !select.classList.contains('error')) {
              totalTilesClickedOk ++;
            }
            // * Row or col is complete
            if(totalTilesClickedOk === totalClues) {
              tilesToCheck.map((tile, index) =>{
                let clicked = document.getElementById(tile).dataset.clicked;
                let fill = document.getElementById(tile).classList.contains('fill');
                if(clicked == 'false' && !fill) {
                  // * This timer make finished line sequentially.
                  setTimeout(() => {
                    this._render.addClass(document.getElementById(tile), 'void');
                    document.getElementById(tile).dataset.clicked = 'true';
                  }, 50 * index);
                }
              });
              const clues = type === 'row' ? document.getElementById(`leftClues${index}`) : document.getElementById(`upperClues${index}`);
              this._render.addClass(clues, 'completed');
            }   
          }, 100);
        }); 
      }
    });
  }

  /**
   * * Redirects route to /home
   */
  backHome() {
    this._render.removeClass(document.querySelector('.toolbar'), 'hide');
    this._router.navigateByUrl('/home');
  }

  /**
   * * Checks if all the tiles are clicked, if that, game is finished.
   */
  checkIfFinished(): void {

    let totalTilesClicked = 0;
    this.boardTile.map(item => {
      if(item._element.nativeElement.dataset.clicked === 'true') {
        totalTilesClicked ++;
      }
      if(totalTilesClicked === this._propertiesService.totalNumberOfTiles && this._propertiesService.lives.length >= 1) {
        // * Timer
        this._propertiesService.timerRunning = false;
        this._propertiesService.timerValue = this.timeCounterComponent.minutes + ':' + this.timeCounterComponent.seconds;

        let message = this._propertiesService.gameType === 'easy' 
          ? this._language.translations.boardGame.dialog.win.easyMessage 
          : this._language.translations.boardGame.dialog.win.hardMessage; 
        this.openDialog(this._language.translations.boardGame.dialog.win.title, message, true);
      }
    });
  }

  /**
   * * Opens a dialog showing if user wins or loses.
   * @param  {String}  title     Title of message
   * @param  {String}  message   Message of win or lose
   * @param  {boolean} hasWon    Boolean var that indicates if user won true | false
   */
  openDialog(title: string, message: string, hasWon: boolean): void {

    const dialogRef = this.dialog.open(ResultDialogComponent, {
      width: '250px',
      disableClose : true,
      data: {
        title: title, 
        message: message,
        rating: this._propertiesService.rating,
        time: this._propertiesService.timerValue,
        board: this._propertiesService.allIdsArray,
        boardID: this._propertiesService.boardID,
        hasWon: hasWon,
        type: this._propertiesService.gameType,
        hasBeenScored: this._propertiesService.isActualBoardScored
      }
    });

    // * Shows again toolbar if is mobile at home
    dialogRef.afterClosed().subscribe(result => {
      this._render.removeClass(document.querySelector('.toolbar'), 'hide');
    });
  }

  /**
   * * Changes isSliderChecked property value.
   */
  slideChange(): void {
    this._propertiesService.isSliderChecked = this.slider.checked;
  }

  /**
   * * Changes mat slider clicking left option.
   */
  slideClick(): void {
    this.slider.checked = this.slider.checked === true ? false : true;
    this._propertiesService.isSliderChecked = this.slider.checked;
  }

  /**
   * * Opens a sanckBar showing if users choice is correct or not.
   * @param  {String} message    Message to show
   * @param  {String} action     Action if proceed
   * @param  {String} type       Type of users choice, success or error
   */
  openSnackBar(message: string, action: string, type: string, ): void {
    // * Config Material SnackBar.
    const config = new MatSnackBarConfig();
    config.panelClass = type === 'success' ? ['success-snackbar'] : ['error-snackbar']
    config.duration = 500;
    config.verticalPosition = 'top';
    this._snackBar.open(message, action, config);
  }
}
