import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// * Components
import { GameBoardComponent } from './game-board/game-board.component';
import { HomeComponent } from './home/home.component';
// * Services
import { TranslationsService } from '../app/services/translations.service';
import { ConfigService } from './services/config.service';

// * Material modules
import { 
  MatButtonModule, 
  MatGridListModule, 
  MatSlideToggleModule,
  MatIconModule, 
  MatSnackBarModule,
  MatDialogModule,
  MatDividerModule,
  MatInputModule,
  MatFormFieldModule,
  MatProgressSpinnerModule
} from '@angular/material';
// * Components
import { ResultDialogComponent } from './result-dialog/result-dialog.component';
import { FormsModule } from '@angular/forms';
import { RulesComponent } from './rules/rules.component';
import { HttpClientModule } from '@angular/common/http';
import { TimeCounterComponent } from './time-counter/time-counter.component';
// * Firebase modules import:
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { FirebaseService } from '../app/services/firebase.service';
import { HallOfFameComponent } from './hall-of-fame/hall-of-fame.component';
import { ScoresDetailComponent } from './hall-of-fame/scores-detail/scores-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    GameBoardComponent,
    HomeComponent,
    ResultDialogComponent,
    RulesComponent,
    TimeCounterComponent,
    HallOfFameComponent,
    ScoresDetailComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatDialogModule,
    MatDividerModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    TranslationsService,
    FirebaseService,
    ConfigService
  ],
  entryComponents: [ResultDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
