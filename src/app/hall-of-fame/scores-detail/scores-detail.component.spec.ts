import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoresDetailComponent } from './scores-detail.component';

describe('ScoresDetailComponent', () => {
  let component: ScoresDetailComponent;
  let fixture: ComponentFixture<ScoresDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoresDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoresDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
