// * Core:
import { Component, OnInit, QueryList, ViewChildren, ElementRef, Renderer2 } from '@angular/core';
// * Router:
import { ActivatedRoute } from "@angular/router";
// * Services:
import { TranslationsService } from 'src/app/services/translations.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-scores-detail',
  templateUrl: './scores-detail.component.html',
  styleUrls: ['./scores-detail.component.scss']
})
export class ScoresDetailComponent implements OnInit {

  @ViewChildren('listItem') listItem: QueryList<ElementRef>

  public board: any;
  public columns: number;
  public boardID: string;
  public isLoading: boolean = true;
  public scores: any = [];
  public orderedScores: any = [];
  public stars: any = [];
  public times: any = [];

  constructor(
    private _language: TranslationsService,
    private _firebase: FirebaseService,
    private _route: ActivatedRoute,
    private _render: Renderer2
  ) { }

  ngOnInit() {
    this.boardID = this._route.snapshot.paramMap.get('id');
    new Promise(resolve =>{
      this._firebase.getBoard(this.boardID).valueChanges().subscribe(item => {
        this.board = item.value;
        this.scores = item.users;
        this.columns = item.value.length === 100 ? 10 : 15;
        resolve();
      }); 
    }).then(() => {
      this.orderScores();
    });
  }

  /**
   * * Orders scores result, first by number of stars then by time
   */
  orderScores() {
    this.scores.sort((a, b) => (a.rating > b.rating) ? -1 
      : (a.rating === b.rating) 
      ? ((a.time > b.time) ? 1 : -1) : 1);
    this.isLoading = false;
    this.findUserScore();
    // * Limits board scores to 10.
    if(this.scores.length > 10) {
      this.scores.length = 10;
      this._firebase.updateBoard(this.boardID, this.scores);
    } 
  }

  /**
   * * Finds user score by alias & highlight it.
   */
  findUserScore() { 
    // * Wait for render the users list
    setTimeout(() => {
      this.listItem.map(item => {
        const it = item.nativeElement;
        if(localStorage.getItem('alias') === it.childNodes[2].innerHTML) {
          this._render.addClass(item.nativeElement, 'userScore');
        }
      });
    }, 50); 
  }
}
