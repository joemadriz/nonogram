// * Core: 
import { Component, OnInit, ViewChildren, QueryList, ElementRef, Renderer2 } from '@angular/core';
// * Services:
import { TranslationsService } from '../services/translations.service';
import { FirebaseService } from '../services/firebase.service';
// * Router:
import { Router } from '@angular/router';

@Component({
  selector: 'app-hall-of-fame',
  templateUrl: './hall-of-fame.component.html',
  styleUrls: ['./hall-of-fame.component.scss']
})
export class HallOfFameComponent implements OnInit {

  @ViewChildren('listItem') listItem: QueryList<ElementRef>

  public isLoading: boolean = true;
  public boards: any = [];

  constructor(
    private _language: TranslationsService,
    private _firebase: FirebaseService,
    private _router: Router,
    private _render: Renderer2
  ) { }

  ngOnInit() {
    new Promise(resolve => {
      this._firebase.getBoards().valueChanges().subscribe(item => {
        this.boards = item;
        resolve();
      });
    }).then(()=> {
      this.sortThreeFirstScores();
    }); 
  }

  /**
   * * Sorts the first best three scores, first by number of stars then by time.
   */
  sortThreeFirstScores() {
    this.boards.map(board => {
      board.users.sort((a, b) => (a.rating > b.rating) ? -1 
      : (a.rating === b.rating) 
      ? ((a.time > b.time) ? 1 : -1) : 1);
      if(board.users.length > 3) {
        board.users.length = 3;
      }
    });
    this.isLoading = false;
    // * Sorts boards by type, first easy then hard.
    this.boards.sort((a,b) => (a.type > b.type ) ? 1 : -1);
  }
  /**
   * * Finds user score by alias & highlight it.
   */
  findUserScore() { 
    // * Wait for render the users list
    setTimeout(() => {
      this.listItem.map(item => {
        const it = item.nativeElement;
        if(localStorage.getItem('alias') === it.childNodes[2].innerHTML) {
          this._render.addClass(item.nativeElement, 'userScore');
        }
      });
    }, 50); 
  }
  /**
   * * Redirects to board scores detail.
   */
  goToBoardDetail(boardID) {
    this._router.navigateByUrl(`/hall-of-fame/${boardID}`);
  }
}
