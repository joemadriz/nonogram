import { Component, OnInit } from '@angular/core';
import { TranslationsService } from '../services/translations.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {

  constructor(
    private _language: TranslationsService
  ) { }

  ngOnInit() {
  }

}
