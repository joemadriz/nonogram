import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-time-counter',
  templateUrl: './time-counter.component.html',
  styleUrls: ['./time-counter.component.scss']
})
export class TimeCounterComponent implements OnInit {

  @ViewChild('secondsSpan', {static: false}) secondsSpan: ElementRef;
  @ViewChild('minutesSpan', {static: false}) minutesSpan: ElementRef;

  public seconds: any = 0;
  public minutes: any = 0;
  public hours: any = 0;
  public minutesCounter = 60;
  public firstTime: boolean = true;
  public timeHasMinutes: boolean = false;
  public timeHasHours: boolean = false;

  @Input() public isTimerRunning: boolean;
  

  constructor() { }

  ngOnInit() {
    this.looPTimer();
  }

  /**
   * * Timer every one second
   */
  looPTimer() {
    if(this.isTimerRunning) {
      if(this.firstTime) {
        this.firstTime = false;
        this.minutes = '0' + this.minutes;
        this.hours = '0' + this.hours;
      }
      else {
        this.seconds ++;
      }
      if(this.seconds < 10) {
        this.seconds = '0' + this.seconds;
      }
      if(this.seconds === this.minutesCounter) {
        this.minutes++;
        this.seconds = '0' + 0;
        if(this.minutes < 10) {
          this.minutes = '0' + this.minutes;
        }
      }
      if(this.minutes === this.minutesCounter) {
        this.hours++;
        this.minutes = '0' + 0;
        if(this.hours < 10) {
          this.hours = '0' + this.hours;
        }
      }
      setTimeout(()=>{
        this.looPTimer();
      }, 1000);
    }
  }
}
