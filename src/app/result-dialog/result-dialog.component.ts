import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
// * Material dialog:
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../game-board/game-board.component';
// * Services:
import { TranslationsService } from '../services/translations.service';
import { FirebaseService } from '../services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html',
  styleUrls: ['./result-dialog.component.scss']
})
export class ResultDialogComponent implements OnInit {

  @ViewChild('ratingInput', {static: false}) ratingInput: ElementRef;
  
  // * Actual board
  public boardUsers: any = [];

  constructor(
    public dialogRef: MatDialogRef<ResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _language: TranslationsService,
    private _firebase: FirebaseService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  /**
   * * Sends user rating to base data.
   */
  sendRating() {
    localStorage.setItem("alias", this.ratingInput.nativeElement.value.toUpperCase());
    if(this.data.hasBeenScored) {
      // * Board has scored before, pushes user name time and score.
      new Promise(resolve => {
        this._firebase.getBoard(this.data.boardID).valueChanges().subscribe(item =>{
          this.boardUsers = item.users;
          this.boardUsers.push({
            name: this.ratingInput.nativeElement.value.toUpperCase(),
            rating: this.data.rating.length,
            time: this.data.time
          });
          resolve();
        });
      }).then(()=>{
        // ! FIREBASE updateBoard()
        this._firebase.updateBoard(this.data.boardID, this.boardUsers);
        this._router.navigateByUrl(`/hall-of-fame/${this.data.boardID}`);
      });
    }
    else {
      // * Board hasn´t scored yet, pushes new board and user name, time and score.
      const board = {
        value: this.data.board,
        type: this.data.type,
        users: [
          {
            name: this.ratingInput.nativeElement.value.toUpperCase(),
            rating: this.data.rating.length,
            time: this.data.time
          }
        ]
      }
      // ! FIREBASE addBoards()
      this._firebase.addBoard(board);
      this._router.navigateByUrl(`/hall-of-fame/${this._firebase.boardId}`);
    }
  }
}
