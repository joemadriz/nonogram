import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TranslationsService {

  public translations: any = {};

  constructor(
    private _http: HttpClient
  ) {
    
    /**
     * * Gets language from navigator language.
     */
    let userLang = navigator.language.slice(0,2); 

    if(userLang !== 'es' && userLang !== 'en') {
      userLang = 'en';
    }    
    // * Chooses a json with navigator language code
    this._http.get(`../../assets/json/${userLang}.json`).subscribe(
      data => {
        this.translations = data;
        this.translations = this.translations.translations;
      }
    );
    
   }
}
