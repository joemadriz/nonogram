import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import * as firebase from 'firebase/app';



interface Board{
  $key?: string;
  value?: string;
  type?: string;
  users?: object;
}

@Injectable({
  providedIn: 'root'
})


export class FirebaseService {

  public gameBoards: any;
  public board: AngularFireObject<Board>;
  public board2: any;
  public boards: AngularFireList<Board[]>;
  public boards2:any;
  public boardIDs: any = [];
  public boardId: string;
  
  constructor(
    private dataBase: AngularFireDatabase
  ) {}
  
  /**
   * * Gets all boards.
   */
  getBoards(): AngularFireList<Board[]> {
    this.boards = this.dataBase.list('/game-boards') as AngularFireList<Board[]>;
    return this.boards;
  }
  /**
   * * Gets a boards by id.
   * @param {String} id  Id from board
   */
  getBoard(id): AngularFireObject<Board> {
    this.board = this.dataBase.object('/game-boards/' + id) as AngularFireObject<Board>;
    return this.board;  
  }
  /**
   * * Updates users object at a board by id.
   * @param {String} id    Id from board
   * @param {Object} users Object users with the new score
   */
  updateBoard(id, users): void {
    this.dataBase.object('/game-boards/' + id).update({
      users: users
    })
  }
  /**
   * * Adds board to database.
   * @param {Object} board Object with new board and user score
   */
  addBoard(board): void {
    const newPostRef = this.dataBase.list('/game-boards').push(board);
    this.boardId = newPostRef.key;
    this.addBoardID(this.boardId);
  }
  /**
   * * Adds key from addBoard push to just created board id property.
   * @param {String} id Object with new board and user score
   */
  addBoardID(id): void {
    this.board2 = this.dataBase.object('/game-boards/' + id);
    this.board2.update({'id': id});
  }
}

