import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {
  // * Dialog data properties.
  public title: string;
  public message: string;
  public time: string;
  // * Slider properties.
  public checked: boolean = true;
  public isSliderChecked: boolean = true;
  // * Board properties.
  public totalNumberOfTiles: number = 0;
  public boardArray: any = [];
  public gameType: string;
  public columns: number;
  public rows: number;
  public upperCluesRowHeight: string;
  public leftCluesRowHeight: string;
  public isActualBoardScored: boolean = false;
  public isBoardReady: boolean = false;
  public boardID: string;
  // * Columns and rows array properties.
  public colResultArray: any = [];
  public colArray: any = [];
  public rowResutArray = []
  public rowArray: any = [];
  public rowCounterArray = [];
  public allIdsArray: any = [];
  // * Tiles index arrays.
  public allIndexRowArray = [];
  public allIndexColArray = [];
  public lives: any = ['favorite', 'favorite', 'favorite'];
  public numOfLives: number = 3;
  public rating: any = ['star', 'star', 'star'];
  public isMobile: boolean = false;
  // * Timer properties
  public timerRunning: boolean = true;
  public timerValue: string;
  // * Firebase properties
  public boards: any;
  
  constructor() { }
}
