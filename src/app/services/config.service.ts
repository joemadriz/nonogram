import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  // ! Use ONLY if your board database is empty.
  // ! If value is true adds a new board to database with hardcoded user score.
  public addBoardToDBatInit: boolean = false;

  constructor() { }

  /**
    * * Adds path property to Event.propertype
    * * Adds equals property to Array.prototype for comparing two arrays.
    */
  loadConfig() {
    /**
    * * Event.path doesn´t exist on firefox and safari
    * * This adds path property to Event.prototype
    */
    if (!("path" in Event.prototype))
    Object.defineProperty(Event.prototype, "path", {
      get: function() {
        var path = [];
        var currentElem = this.target;
        while (currentElem) {
          path.push(currentElem);
          currentElem = currentElem.parentElement;
        }
        if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
          path.push(document);
        if (path.indexOf(window) === -1)
          path.push(window);
        return path;
      }
    });

    /**
     * * This adds equals property to Array.prototype for comparing two arrays.
     * * Use it like that: arr1.equals(arr2) returns true or false.
     */  
    if(!('equals' in Array.prototype)) {
      Array.prototype['equals'] = function (array) {
        // if the other array is a falsy value, return
        if (!array)
          return false;
        // compare lengths - can save a lot of time 
        if (this.length != array.length)
          return false;
        for (var i = 0, l=this.length; i < l; i++) {
          // Check if we have nested arrays
          if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
          }           
          else if (this[i] != array[i]) { 
              return false;   
          }           
        }       
        return true;
      }
    }

    // Hide method from for-in loops
    Object.defineProperty(Array.prototype, "equals", {enumerable: false});
  }
}
