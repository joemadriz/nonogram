import { NgModule } from '@angular/core';
// * Router:
import { Routes, RouterModule } from '@angular/router';
// * Import of components
import { HomeComponent } from './home/home.component';
import { GameBoardComponent } from './game-board/game-board.component';
import { RulesComponent } from './rules/rules.component';
import { HallOfFameComponent } from './hall-of-fame/hall-of-fame.component';
import { ScoresDetailComponent } from './hall-of-fame/scores-detail/scores-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' }, 
  { path: 'home', component: HomeComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'hall-of-fame', component: HallOfFameComponent },
  { path: 'hall-of-fame/:id', component: ScoresDetailComponent }, 
  { path: 'game-board/:type', component: GameBoardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
