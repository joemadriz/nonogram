// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAXQQPy4NGt47AxdwrW2NlkstgdrzF1rcw",
    authDomain: "nonogram-fb406.firebaseapp.com",
    databaseURL: "https://nonogram-fb406.firebaseio.com/",
    projectId: "nonogram-fb406",
    storageBucket: "nonogram-fb406.appspot.com",
    messagingSenderId: "440696033084",
    appId: "1:440696033084:web:e016848e9d3c2361365839",
    measurementId: "G-GZZSWV9KSE"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
